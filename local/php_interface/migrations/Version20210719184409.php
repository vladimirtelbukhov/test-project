<?php

namespace Sprint\Migration;

use Bitrix\Highloadblock\HighloadBlockLangTable;
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Loader;

class Version20210719184409 extends Version
{
    protected $description = "Create user addresses Highload-block  ";

    protected $moduleVersion = "3.28.7";

    private $hbId = null;

    public function up()
    {
        $this->initModules();
        $this->createHighloadBlock();
        $this->createHighloadBlockLang();
        $this->addUserFields();
    }

    public function down()
    {
        $this->initModules();
        $this->deleteUserFields();
        $this->deleteHighloadBlockLang();
        $this->deleteHighloadBlock();
    }

    private function createHighloadBlock()
    {
        $result = HighloadBlockTable::add([
            "NAME" => 'UserAddresses',
            "TABLE_NAME" => 'user_addresses',
        ]);
        if (!$result->isSuccess()) {
            throw new \Exception("Highload block adding error.");
        }
        $this->hbId = $result->getId();
        $this->saveData("HB_ID", $this->hbId);
    }

    private function createHighloadBlockLang()
    {
        $arLangs = [
            'ru' => 'Адреса пользователя',
            'en' => 'User addresses',
        ];
        $langTableResult = [];
        foreach ($arLangs as $key => $val) {
            $result = HighloadBlockLangTable::add([
                "ID" => $this->hbId,
                "LID" => $key,
                "NAME" => $val,
            ]);
            $langTableResult[] = $result->getId();
        }
        $this->saveData("HL_LANG_TABLE_ID", $langTableResult);
    }

    private function addUserFields()
    {
        $ufObject = "HLBLOCK_" . $this->hbId;

        $arFields = [
            [
                "ENTITY_ID" => $ufObject,
                "FIELD_NAME" => "UF_USER",
                "USER_TYPE_ID" => "integer",
                "XML_ID" => "UF_USER",
                "EDIT_FORM_LABEL" => [
                    "ru" => "Пользователь",
                    "en" => "User",
                ],
                "LIST_COLUMN_LABEL" => [
                    "ru" => "Пользователь",
                    "en" => "User",
                ],
                "LIST_FILTER_LABEL" => [
                    "ru" => "Пользователь",
                    "en" => "User",
                ],
            ],
            [
                "ENTITY_ID" => $ufObject,
                "FIELD_NAME" => "UF_ADDRESS",
                "USER_TYPE_ID" => "string",
                "XML_ID" => "UF_ADDRESS",
                "EDIT_FORM_LABEL" => [
                    "ru" => "Адрес",
                    "en" => "Address",
                ],
                "LIST_COLUMN_LABEL" => [
                    "ru" => "Адрес",
                    "en" => "Address",
                ],
                "LIST_FILTER_LABEL" => [
                    "ru" => "Адрес",
                    "en" => "Address",
                ],
            ],
            [
                "ENTITY_ID" => $ufObject,
                "FIELD_NAME" => "UF_ACTIVE",
                "USER_TYPE_ID" => "boolean",
                "XML_ID" => "UF_ACTIVE",
                "EDIT_FORM_LABEL" => [
                    "ru" => "Активность",
                    "en" => "Active",
                ],
                "LIST_COLUMN_LABEL" => [
                    "ru" => "Активность",
                    "en" => "Active",
                ],
                "LIST_FILTER_LABEL" => [
                    "ru" => "Активность",
                    "en" => "Active",
                ],
            ],
        ];

        $arAddedFieldsId = [];
        $obUserField = new \CUserTypeEntity;
        foreach ($arFields as $arField) {
            $arAddedFieldsId[] = $obUserField->Add($arField);
        }
        $this->saveData("UF_IDS", $arAddedFieldsId);
    }

    private function deleteUserFields()
    {
        $arUserFieldsId = $this->getSavedData("UF_IDS");
        $obUserField = new \CUserTypeEntity;
        foreach ($arUserFieldsId as $item) {
            $obUserField->Delete($item);
        }
    }

    private function deleteHighloadBlockLang()
    {
        $arLangTableIds = $this->getSavedData("HL_LANG_TABLE_ID");
        foreach ($arLangTableIds as $arLangTableId) {
            HighloadBlockLangTable::delete($arLangTableId);
        }
    }

    private function deleteHighloadBlock()
    {
        $hbId = $this->getSavedData("HB_ID");
        HighloadBlockTable::delete($hbId);
    }

    private function initModules()
    {
        if (!Loader::includeModule('highloadblock')) {
            throw new \Exception("Highload block module does not install.");
        }
    }
}
