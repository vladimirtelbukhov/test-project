<?

use Bitrix\Main\Application;

$docRoot = Application::getDocumentRoot();

// autoloader

spl_autoload_register(function ($class) use ($docRoot) {
    if (class_exists($class) || interface_exists($class)) {
        return;
    }
    $class = str_replace('\\', '/', $class);
    $pos = strpos($class, ".");
    if ($pos !== false) {
        return;
    }
    $pathToClass = $docRoot . "/local/lib/" . ucfirst($class) . '.php';

    if (file_exists($pathToClass)) {
        require_once $pathToClass;
    }
});