<?

$eventManager = \Bitrix\Main\EventManager::getInstance();

// event type is <EntityName><EventName>

$eventManager->addEventHandler('', 'UserAddressesOnBeforeAdd', [Handlers\HighloadBlock::className(), "componentCacheClear"]);
$eventManager->addEventHandler('', 'UserAddressesOnBeforeUpdate', [Handlers\HighloadBlock::className(), "componentCacheClear"]);
$eventManager->addEventHandler('', 'UserAddressesOnBeforeDelete', [Handlers\HighloadBlock::className(), "componentCacheClear"]);