<?

namespace Handlers;

/**
 * Class HighloadBlock
 * @package Handlers
 */
class HighloadBlock extends BaseHandler
{
    /**
     * @param $fields
     */
    public function componentCacheClear($fields)
    {
        \CBitrixComponent::clearComponentCache("test:addresses.list");
    }
}