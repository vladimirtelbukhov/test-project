<?php


namespace Handlers;

/**
 * Class BaseHandler
 * @package Handlers
 */
class BaseHandler
{
    /**
     * @return string
     */
    static public function className()
    {
        return get_called_class();
    }
}