<? use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentParameters = [
    "PARAMETERS" => [
        "IS_ONLY_ACTIVE" => [
            "NAME" => Loc::getMessage("IS_ONLY_ACTIVE"),
            "TYPE" => "CHECKBOX"
        ]
    ]
];