<?php


use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

class AddressesList extends CBitrixComponent
{
    /** @var array $errors */
    private array $errors = [];

    /**
     * @throws \Bitrix\Main\LoaderException
     */
    public function executeComponent()
    {
        $this->initModules();
        if (count($this->errors) > 0) {
            ShowError(current($this->errors));
            return;
        }
        if (!$this->_user()->IsAuthorized()) {
            $this->errors[] = Loc::getMessage("USER_IS_NOT_AUTHORIZED");
        }
        if (count($this->errors) > 0) {
            ShowError(current($this->errors));
            return;
        }
        if ($this->startResultCache()) {
            $this->initArResult();
            $this->includeComponentTemplate();
        }
    }

    /**
     * initialize component result array
     */
    private function initArResult()
    {
        $this->arResult["ITEMS"] = $this->getItems();
        if (count($this->errors) > 0) {
            ShowError(current($this->errors));
            $this->abortResultCache();
        }
    }

    /**
     * @return array
     */
    private function getItems(): array
    {
        $arItems = [];

        // get highload block id
        $hblock = HighloadBlockTable::getList([
            "select" => ["ID"],
            "filter" => [
                "NAME" => "UserAddresses"
            ]
        ])->fetch();
        if (!$hblock["ID"]) {
            $this->errors[] = Loc::getMessage("THERE_IS_NO_HIGHLOAD_BLOCK");
            return $arItems;
        }

        // get entity data class
        $hbTable = HighloadBlockTable::getById($hblock["ID"])->fetch();

        $entity = HighloadBlockTable::compileEntity($hbTable);
        $entityDataClass = $entity->getDataClass();

        $filter = [
            "UF_USER" => $this->_user()->GetID()
        ];
        if (isset($this->arParams["IS_ONLY_ACTIVE"]) && $this->arParams["IS_ONLY_ACTIVE"] === "Y") {
            $filter["UF_ACTIVE"] = 1;
        }

        // get list from highload block
        $dbRes = $entityDataClass::getList([
            'filter' => $filter,
            'select' => ["UF_ADDRESS"]
        ]);
        while ($arRes = $dbRes->fetch()) {
            $arItems[] = [
                "ADDRESS" => $arRes["UF_ADDRESS"]
            ];
        }
        return $arItems;
    }

    /**
     * @throws \Bitrix\Main\LoaderException
     */
    private function initModules()
    {
        if (!Loader::includeModule('highloadblock')) {
            $this->errors[] = Loc::getMessage("HIGHLOADBLOCK_MODULE_NOT_INSTALLED");
        }
    }

    /**
     * @return CUser
     */
    private function _user()
    {
        global $USER;
        return $USER;
    }
}