<? use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CMain $APPLICATION
 * @var array $arResult
 */

if (!empty($arResult["ITEMS"])): ?>
    <table>
        <thead>
        <tr>
            <th><?= Loc::getMessage("AL_ADDRESS") ?></th>
        </tr>
        </thead>
        <tbody>
        <? foreach ($arResult["ITEMS"] as $item): ?>
            <tr>
                <td><?= $item["ADDRESS"] ?></td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>
<? endif;