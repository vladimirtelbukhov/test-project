<? use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = array(
    "NAME" => Loc::getMessage("ADDRESSES_LIST_COMPONENT_NAME"),
    "DESCRIPTION" => Loc::getMessage("ADDRESSES_LIST_COMPONENT_DESCRIPTION"),
    "PATH" => [
        "ID" => "content",
    ]
);